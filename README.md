# README

*medite* est une démonstration des outils json-editor et pouchdb.

## Installation
La branche *master* désigne la version stable du logiciel, tandis que les branches *dev* et *dist* désignent
respectivement la version de développement et la version *pré-construite* qui représente en général l'état de *dev*,
pratique si vous n'avez pas **yeoman** sous la main.


### Le plus simple

Une démo se trouve à [http://medite.waglo.com/](http://medite.waglo.com/) mais vous pouvez facilement le tester chez vous:

```
git clone https://gitorious.org/medite/medite.git
cd medite
git branch # on connait la branche master explicitement, mais les autres sont là aussi (git branch -a)
git checkout dist
git branch # on connait maintenant explicitement la branche dist aussi
cd dist
php -S localhost:9000 # utiliser n'importe quel serveur web ici
firefox http://localhost:9000 # utiliser n'importe quel fureteur moderne ici
```

### Avec yeoman
Pour construire medite ou pour y faire du développement, l'outil **yeoman** est nécessaire. Celui-ci repose aussi sur **bower**
et **npm** qui reposent eux-même sur **NodeJS**.

En présumant que vous avez les outils nécesaires:

```
git clone https://gitorious.org/medite/medite.git
cd medite
git checkout dev # on présume qu'on va travailler avec la version dev
npm install # s'occupe des dépendances de développement
bower install # s'occupe des dépendances clients (JavaScript et CSS)
cd test
bower install # nécessaires pour les tests
cd - # retourner au répertoire précédent
grunt serve # démarre un serveur web avec l'application accessible à http://localhost:9000
CTRL-C # pour terminer
```

Avec *grunt serve*, vous pouvez éditer les souces et la page sera automatiquement rechargée dans le fureteur dès que les changements seront sauvés.

Satisfait, vous pouvez générer une version minifiée, avec des fichiers JavaScript et CSS concaténés, etc.:

```
grunt # produire une version de production dans le répertoire dist/
```
