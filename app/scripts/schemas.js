/*jshint unused:false */

function getSchemas() {
    'use strict';
    return {
        'n/a': {
            type: 'object',
            title: 'yup',
            format: 'grid',
            properties: {
                oy: {
                    type: 'string'//, format: 'markdown'
                },
                _id: {
                    readonly: true
                },
                _rev: {
                    readonly: true
                }
            }
        },
        wikipage: {
            type: 'object',
            title: 'Wiki page',
//            format: 'grid',
            properties: {
                title: {
                    type: 'string'
                },
                content: {
                    type: 'string',
                    format: 'textarea',
                    description: 'Format markdown supporté'
                },
                _id: {
                    readonly: true
                },
                _rev: {
                    readonly: true
                },
                wiki: {
                    type: 'boolean',
                    'default': true,
                    readonly: true
                },
                links: {
                    type: 'array',
                    options: {
                        hidden: true
                    }
                }
            }
        },
        page: {
            type: 'object',
            title: 'Page',
            format: 'grid',
            properties: {
                oy: {
                    type: 'string'//, format: 'markdown'
                },
                _id: {
                    readonly: true
                },
                _rev: {
                    readonly: true
                }
            }
        },
        org: {
            type: 'object',
            title: 'Organisation',
            format: 'grid',
            properties: {
                oy: {
                    type: 'string'//, format: 'markdown'
                },
                _id: {
                    readonly: true
                },
                _rev: {
                    readonly: true
                }
            }
        },
        person: {
            type: 'object',
            title: 'Person',
            format: 'grid',
            properties: {
                oy: {
                    type: 'string',//, format: 'markdown'
                    description: 'une description de champ'
                },
                _id: {
                    readonly: true
                },
                _rev: {
                    readonly: true
                }
            }
        },
        schema: {
            '$ref': '/schemas/schema'
        },
        card: {
            '$ref': '/schemas/card',
            properties: {
                _id: {
                    readonly: true,
                    description: 'preset'
                },
                _rev: {
                    readonly: true,
                    description: 'automatic'
                }
            }
        },
        address: {
            '$ref': '/schemas/address'
        },
        geo: {
            '$ref': '/schemas/geo'
        },
        calendar: {
            '$ref': '/schemas/calendar'
        }
    };
}
