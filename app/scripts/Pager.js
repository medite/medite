/* globals _ */

'use strict';

var Pager = function (id, perpage) {
    var self = this;
    this.id = id;
    this.pages = [];
    this.current = 0;
    this.lastLimit = 0;
    this.PERPAGE = perpage||10;

    $('#' + id + ' .pager .suivants').on('click', 'a', function(e) {
        self.willClick('forward', e, this);
    });

    $('#' + id + ' .pager .precedents').on('click', 'a', function(e) {
        self.willClick('backward', e, this);
    });
};

Pager.prototype.setLast = function(limit) {
    this.lastLimit = limit;
};

Pager.prototype.invalidate = function(id) {
    var r;

    for (r = 1; r < this.pages.length; ++r) {
        if (id < this.pages[r]) {
            this.pages = this.pages.slice(0, r - 1);
            this.lastLimit = 0;
        }
    }
};

Pager.prototype.willClick = function(direction, e, t) {
    e.preventDefault();
    if (!$(t).parent().hasClass('disabled')) {
        this.update(direction);
    }
};

Pager.prototype.opts = function(direction) {
    var opts = { limit: this.PERPAGE};

    if ('undefined' === typeof direction) { direction = false; }

    if ('forward' === direction) {
        ++this.current;
    } else if ('backward' === direction) {
        --this.current;
    }

    if (this.lastLimit && (this.current === this.pages.length - 1)) {
        opts.limit = this.lastLimit;
    } else if (!direction && (!this.pages.length && !this.current)) {
        this.pages.push('');
        ++opts.limit;
    } else if (('forward' === direction) && (this.current + 1 <= this.pages.length)) {
        ++opts.limit;
    } else if ('backward' === direction) {
        ++opts.limit;
    }

    opts.startkey = this.pages[this.current];

    return opts;
};

Pager.prototype.resultsNormal = function(d) {
    var extra;
    if (d.rows.length > this.PERPAGE) {
        extra = d.rows.pop();
        if (!this.lastLimit) {
            this.pages.push(extra.key);
        }
    } else if (d.rows.length <= this.PERPAGE) {
        if (!this.lastLimit) {
            this.setLast(d.rows.length);
        }
    }

    this.ack(d);
};

Pager.prototype.ack = function(d, keep, more) {
    if (!keep) {
        $('#' + this.id + ' ol').empty();
    }
    if (!more) {
        if (this.current) {
            $('#' + this.id + ' .pager .precedents').removeClass('disabled');
        } else {
            $('#' + this.id + ' .pager .precedents').addClass('disabled');
        }

        if (this.lastLimit && (this.current === this.pages.length - 1)) {
            $('#' + this.id + ' .pager .suivants').addClass('disabled');
        } else {
            $('#' + this.id + ' .pager .suivants').removeClass('disabled');
        }
    }

    if ('docs' === this.id) {
        $('#docs ol').append(_.templates.docs(d));
    } else if ('other' === this.id) {
        $('#other ol').append(_.templates.others(d));
    }
};

Pager.prototype.resultsFirst = function(d, opts, db) {
    var extra = false, self = this;
    if (d.rows.length === this.PERPAGE + 1) {
        extra = d.rows.pop();
        if (!self.lastLimit) {
            if (extra.key > self.pages[self.pages.length - 1]) {
                self.pages.push(extra.key);
            }
        }
        self.ack(d);
    } else if (d.rows.length === this.PERPAGE) {
        db.allDocs({startkey:'_design/\uffff', limit: 1}, function(e2, d2) {
            if (d2.rows.length) {
                if (!self.lastLimit) {
                    if (d2.rows[0].key > self.pages[self.pages.length - 1]) {
                        self.pages.push(d2.rows[0].key);
                    }
                }
            } else if (!self.lastLimit) {
                self.setLast(self.PERPAGE);
            }
            self.ack(d);
        });
    } else {
        self.ack(d, false, true);
        db.allDocs({startkey:'_design/\uffff', limit: 1 + self.PERPAGE - d.rows.length}, function(e2, d2) {
            if (d2.rows.length === 1 + self.PERPAGE - d.rows.length) {
                extra = d2.rows.pop();
                if (!self.lastLimit) {
                    self.pages.push(extra.key);
               }
            } else if (!self.lastLimit) {
                self.setLast(d.rows.length);
            }
            self.ack(d2, true, false);
        });
    }
};
