(function(dbname) {
    /* globals JSONEditor, PouchDB, _, getSchemas, Pager, marked, emit */
    /* jshint camelcase: false */
    'use strict';

    var db = new PouchDB(dbname), dbInfo, wikiDocid,
        SCHEMAS = getSchemas(),
        otherPager = new Pager('other'),
        docsPager = new Pager('docs', 5);

    docsPager.update = function (direction) {
        var opts = this.opts(direction), self = this;

        if (opts.startkey < '_design/') {
            opts.endkey = '_design/';
            db.allDocs(opts, function(e, d) {
                self.resultsFirst(d, opts, db);
            });
        } else if (opts.startkey >= '_design/\uffff') {
            db.allDocs(opts, function(e, d) {
                self.resultsNormal(d, opts);
            });
        }

        $('#docs ol.list-unstyled').css('counter-reset', 'line ' + (this.PERPAGE * (self.current)));
    };

    otherPager.update = function(direction) {
        var opts = this.opts(direction), self = this;

        db.allDocs(opts, function(e, d) {
            self.resultsNormal(d, opts);
        });

        $('#other ol.list-unstyled').css('counter-reset', 'line ' + (this.PERPAGE * (self.current)));
    };

    JSONEditor.defaults.languages.fr = {error_type: 'La valeur doit être de type {{0}}'};
    JSONEditor.defaults.language = 'fr';
    //JSONEditor.plugins.epiceditor.basePath = 'bower_components/epiceditor/epiceditor';

    PouchDB.on('created', function (dbname) {
        if (-1 !== dbname.indexOf('-mrview-temp')) {
            return true;
        }
        $(function() {
            var editor, type;

            $('#docs form select').html(_.templates['schema-select']({schemas:Object.keys(SCHEMAS)}));

            $('.nav-tabs li a').click(function (e) {
                if (!$(this).parent().hasClass('disabled')) {
                    $(this).tab('show');
                } else {
                    e.preventDefault();
                }
            });

            $('#import form').submit(function(e) {
                var v, data, r, useRev = false;
                e.preventDefault();
                v = $(this).serializeArray();

                try {
                    for (r = 0; r < v.length; ++r) {
                        if ('bulk' === v[r].name) {
                                data = JSON.parse(v[r].value);
                        } else if ('useRev' === v[r].name) {
                            useRev = true;
                        }
                    }

                    if (!_.isArray(data)) {
                        data = [data];
                    }

                    if (useRev) {
                        // verifier que le champ _rev est là
                        if (!_.every(data, function(a) {
                            return a._rev;
                        })) {
                            $('#import textarea').val('ERREUR bulkDocs, le champ _rev est introuvable dans au moins un des documents\n\n' + $('#import textarea').val());
                            return;
                        }
                    } else {
                        data = _.map(data, function(a) {
                            delete a._rev;
                            return a;
                        });
                    }

                    db.bulkDocs(data, function(e, d) {
                        if (e) {
                            $('#import textarea').val('ERREUR bulkDocs' + JSON.stringify(e, null, '    ') + '\n\n' + $('#import textarea').val());
                        } else if (d[0] && d[0].error) {
                            $('#import textarea').val('ERREUR bulkDocs' + JSON.stringify(d, null, '    ') + '\n\n' + $('#import textarea').val());
                        } else {
                            $('#import textarea').val('importation terminée avec succès de ' + d.length + ' documents.\n\n' + $('#import textarea').val());
                        }
                    });
                } catch(e) {
                    $('#import textarea').val('ERREUR bulkDocs JSON: ' + e.message + '\n\n' + $('#import textarea').val());
                }
            });
            
            $('#docs form').submit(function(e) {
                var v;
                e.preventDefault();
                v = $('#docs input').val();
                type = $('#docs select').val();
                $('#docs input').val('');
                location.hash = '#/doc/' + v + '/edit';
                // pour permettre au document count de d'updater après la création du doc
                dbInfo = false;
                setTimeout(function() {
                    $('[href="#doc"]').tab('show');
                }, 1);
            });

            $('#export form').submit(function(e) {
                var useRev = $('#export form input[name=useRev]:checked').val();
                e.preventDefault();
                db.allDocs({include_docs: true}, function(e, d) {
                    var data = _.map(d.rows, function(a) {
                        if (!useRev) {
                            delete a.doc._rev;
                        }
                        return a.doc;
                    });

                    $('#export textarea').html(JSON.stringify(data, null, '    ')).focus();
                });
            });

            $('#doc h2 small').on('click', 'a', function(e) {
                var docid;
                e.preventDefault();
                docid = location.hash.slice(6).split('/')[0];
                location.hash = '#/voir/' + docid;
                setTimeout(function() {
                    $('[href="#voir"]').tab('show');
                }, 1);
            });

            $('#voir h2 small.edit').on('click', 'a', function(e) {
                var docid;
                e.preventDefault();
                docid = location.hash.slice(7).split('/')[0];
                location.hash = '#/doc/' + docid + '/edit';
                setTimeout(function() {
                    $('[href="#doc"]').tab('show');
                }, 1);
            });

            $('#voir h2 small.wiki').on('click', 'a', function(e) {
                var docid;
                e.preventDefault();
                docid = location.hash.slice(7).split('/')[0];
                location.hash = '#/wiki';
                setTimeout(function() {
                    $('[href="#wiki"]').tab('show');
                    wiki(docid);
                }, 1);
            });

            $('#docs ol').on('click', 'a', function() {
                setTimeout(function() {
                    $('[href="#voir"]').tab('show');
                }, 1);
            });

            $('#docs ol').on('click', 'button', function(e) {
                var docid;
                e.preventDefault();

                docid = $(this).siblings('a').attr('href').slice(7).split('/')[0];

                location.hash =  '#/doc/' + docid + '/edit';

                // laisse le temps de changer d'URL
                setTimeout(function() {
                    $('[href="#doc"]').tab('show');
                }, 1);
            });

            $('.nav-tabs li a').on('hide.bs.tab', function () {
                var s = $(this).attr('aria-controls');
                if ('doc' === s) {
                    $('#doc form').off('submit');
                    $('#doc form button').off('click');
                    editor.destroy();
                } else if ('wiki' === s) {
                    $('#wiki').removeClass('well');
                }
            });

            $('.nav-tabs li a').on('show.bs.tab', function () {
                var s = $(this).attr('aria-controls'), docid;
                if ('doc' === s) {
                    docid = location.hash.slice(6).split('/')[0];

                    db.get(docid, function(e, d) {
                        var schema;

                        if (e) {
                            schema = SCHEMAS[type];
                            d = {
                                _id: docid,
                                _rev: null,
                                type: type
                            };
                            if (schema.properties && schema.properties.wiki && schema.properties.wiki.default) {
                                d.wiki = true;
                                d.content = '';
                                d.title = d._id.charAt(0).toUpperCase() + d._id.slice(1);
                            }
                        } else if (d.type && SCHEMAS[d.type]) {
                            schema = SCHEMAS[d.type];
                        } else {
                            schema = SCHEMAS.wikipage;
                        }

                        editor = new JSONEditor(document.getElementById('jsed'), {
                            schema: schema,
                            theme: 'bootstrap3',
                            iconlib: 'bootstrap3',
                            startval: d,
                            ajax: true
                        });

                        editor.on('ready', function() {
                            var first = true;

                            editor.on('change', function() {
                                $('#doc input[type="submit"]').attr('disabled', first);
                                if (first) {
                                    first = false;
                                }
                            });

                            $('#doc form button.btn-danger').on('click', function(e) {
                                var id, rev, name;

                                e.preventDefault();
                                name = editor.getEditor('root._rev');
                                if (name) {
                                    rev = name.getValue();
                                }

                                name = editor.getEditor('root._id');
                                if (name) {
                                    id = name.getValue();
                                }

                                db.remove(id, rev, function() {
                                    dbInfo = false;
                                    $('[href="#docs"]').tab('show');
                                });
                            });

                            $('#doc form').on('submit', function(e) {
                                var d = editor.getValue();
                                e.preventDefault();

                                extractLinks(d);
                                
                                db.put(d, function(er, resp) {
                                    var name;

                                    if (!er) {
                                        name = editor.getEditor('root._rev');
                                        if (name) {
                                            name.setValue(resp.rev);
                                            first = true;
                                        }
                                    }
                                });
                            });
                        });
                    });
                } else if ('docs' === s) {
                    docsPager.update();
                } else if ('other' === s) {
                    otherPager.update();
                } else if ('voir' === s) {
                    voir();
                } else if ('wiki' === s) {
                    wiki('accueil');
//                } else if ('export' === s) {
//                    exportBulk();
                }
            });

            $('#wiki').on('click', 'a', function(e) {
                var page = $(this).attr('href');
                e.preventDefault();
                if ('#' === page) {
                    location.hash = '#/doc/' + wikiDocid + '/edit';
                    setTimeout(function() {
                        $('[href="#doc"]').tab('show');
                    }, 1);
                } else {
                    wiki(page);
                }
            });

            function extractLinks(d) {
                if (d && d.wiki && d.content) {
                    d.links = [];
                    $('a', marked(d.content)).each(function(i, e) {
                        d.links.push(e.pathname.slice(1));
                    });
                }
            }
            
            function wiki(page) {
                wikiDocid = page;
                $('#wiki').addClass('well');
                db.get(page, function(e, d) {
                    if (e) {
                        if ('accueil' === page) {
                            type = 'wikipage';
                            location.hash = '#/doc/accueil/edit';
                            setTimeout(function() {
                                $('[href="#doc"]').tab('show');
                            }, 1);
                        } else {
                            type = 'wikipage';
                            location.hash = '#/doc/' + page + '/edit';
                            setTimeout(function() {
                                $('[href="#doc"]').tab('show');
                            }, 1);                            
                        }
                        return;
                    }
                    d.content = marked(d.content);
                    d.hourra = 'coucou!';
                    $('#wiki div.content').html(_.templates.wikipage(d));
                    $('#wiki div.col-md-2 ul').empty();

                    db.query(function(doc) {
                        var r;
                        if (doc.wiki && doc.links && doc.links.length) {
                            for (r = 0; r < doc.links.length; ++r) {
                                if (doc.links[r] !== doc._id) {
                                    emit(doc.links[r], doc.title);
                                }
                            }
                        }
                    }, {key:page}, function(e2, d2) {
                        $('#wiki div.col-md-2 ul').append(_.templates.backlinks(d2));
                  });
                });
            }

            function voir() {
                db.get(location.hash.split('/')[2], function(e, d) {
                    if (e) {
                        $('#voir div').html('erreur:' + JSON.stringify(e, null, ' '));
                        return;
                    }

                    $('#voir h2 small.wiki').html(d && d.type && ('wikipage' === d.type) && d.wiki?'<a href="#">wiki</a>':'');

                    if (d && d.type && _.templates[d.type]) {
                        $('#voir div').html(_.templates[d.type](d));
                    } else {
                        $('#voir div').html(_.templates.oy(d));
                    }
                });
            }

            function updateInfo(d) {
                /* jshint camelcase: false */
                dbInfo.update_seq = d.seq;
                $('.info').html(_.templates.info(dbInfo));
            }

            db.info(function(e, d4) {
                dbInfo = d4;
                updateInfo({seq:d4.update_seq});

                db.changes({since:d4.update_seq, live:true}).on('change', function(d) {

//                    //creation
//                    if (1 == d.changes[0].rev.split('-')[0]) {
//                        ++dbInfo.doc_count;
//                    } else {
//                    //update
//                    }

                    if (d.id) {
                        docsPager.invalidate(d.id);
                    }

                    if (dbInfo) {
                        updateInfo(d);
                    } else {
                        db.info(function(e, d3) {
                            dbInfo = d3;
                            updateInfo(d);
                        });
                    }
                });
            });
            $('.page-header').html(_.templates.allo({nom: dbname}));
        });
    });
}('p-0.0.0')); // pa
